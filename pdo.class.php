<?php
/**
 * Auteur: Omar Abdeljelil
 * Date:   29 juillet 2018
 * Projet: TP2- Gestion d’une collection de livres avec PHP, Mysql, jQuery et Ajax
 * Webdev: https://e1795082.webdev.cmaisonneuve.qc.ca/tp2-ajax/
 */

/**
 * Class PdoDB établit la connexion à la base de données,
 * sécurise l'exécution des requêtes
 * et renvoie les résultats souhaités
 */
class PdoDB
{
    //les variables sont définies dans le fichier modèle
    private $hote = BD_HOTE;
    private $utilisateur = BD_UTILISATEUR;
    private $mot_passe = BD_MOTDEPASS;
    private $nom_bd = BD_NOM;

    private $connexion; // détient la connexion à la base de données
    private $erreur; // contient le message d'erreur
    private $stmt; // détient les déclarations préparées

    /**
     * PdoDB constructor.
     * le constructeur de classe, Crée une instance PDO
     * représentant une connexion à la base de données
     */
    public function __construct()
    {
        // data source name l'hote et le nom de la base de données
        $dsn = "mysql:host=" . $this->hote . ";dbname=" . $this->nom_bd;
        $options = array(
            //vérifier pour voir s'il existe déjà une connexion établie avec la base de données.
            PDO::ATTR_PERSISTENT => true,
            // lancer (throw) une exception si une erreur se produit
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        );

        try {
            // affecter la connexion à la variable
            $this->connexion = new PDO($dsn, $this->utilisateur, $this->mot_passe, $options);
        } //attraper les erreurs
        catch (PDOException $e) {
            $this->erreur = $e->getMessage();
        }
    }

    /**
     * function requete prépare la requête pour sécuriser l'exécution contre les injections sql
     * @param $req est la requête envoyer
     */
    public function requete($req)
    {
        $this->stmt = $this->connexion->prepare($req);
    }

    /**
     * function bind Lie un paramètre au nom de variable spécifié avec son type
     * @param $param le paramètre reçu
     * @param $value la valeur à lier
     * @param null $type de la valeur. mis à null pour entrer dans le test de 'switch'
     */
    public function bind($param, $value, $type = null)
    {
        if (is_null($type)) {
            // set the datatype of the parameter
            switch (true) {
                case is_int($value):
                    $type = PDO::PARAM_INT;
                    break;
                case is_bool($value):
                    $type = PDO::PARAM_BOOL;
                    break;
                case is_null($value):
                    $type = PDO::PARAM_NULL;
                    break;
                default:
                    $type = PDO::PARAM_STR;
            }
        }
        $this->stmt->bindValue($param, $value, $type);
    }

    /**
     * function execute la déclaration préparée
     * @return Bool TRUE en cas de succès ou FALSE en cas d'échec.
     */
    public function execute()
    {
        return $this->stmt->execute();
    }

    /**
     * function resultatsDeRequetes
     * utilise la méthode PDOStatement :: fetchAll PDO pour récupérer toutes les lignes
     * @return mixed renvoie un tableau des lignes de l'ensemble de résultats.
     */
    public function resulatsDeRequetes()
    {
        // Nous exécutons d'abord la méthode execute, puis nous renvoyons les résultats
        $this->execute();
        return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * function seuleRanger
     * renvoie simplement un seul enregistrement de la base de données.
     * @return mixed un tableau indexé par le nom de la colonne
     * tel qu'il est retourné dans le jeu de résultats
     */
    public function seuleRanger()
    {
        $this->execute();
        return $this->stmt->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * function nbrLignes()
     * renvoie le nombre de lignes affectées par la dernière instruction SQL
     * @return Int nombre de lignes
     */
    public function nbrLignes()
    {
        return $this->stmt->rowCount();
    }

    /**
     * function dernierIdInseree
     * Renvoie l'ID de la dernière ligne insérée ou de la dernière valeur de séquence
     * @return String représentant l'identifiant de ligne de la dernière ligne insérée dans la base de données
     */
    public function dernierIdInseree()
    {
        return $this->connexion->lastInsertId();
    }

}