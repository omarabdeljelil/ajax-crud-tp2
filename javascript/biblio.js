/**
 * Auteur: Omar Abdeljelil
 * Date:   29 juillet 2018
 * Projet: TP2- Gestion d’une collection de livres avec PHP, Mysql, jQuery et Ajax
 * Webdev: https://e1795082.webdev.cmaisonneuve.qc.ca/tp2-ajax/
 */

$(document).ready(function () {
    /**
     * function autoComplete
     * récupérer un tableau de mots pour donner des suggestions
     * tout en tapant dans chaque champ de filtre du formulaire.
     * @param champAutoComp le nom de la colonne dans la base de données
     */
    function autoComplete(champAutoComp) {
        var listeAutoComp = [];
        $.ajax({
            url: 'ajax.php',
            type: 'GET',
            dataType: "json",
            data: {
                champ: champAutoComp, // envoyer le nom de la colonne
                action: 'autoComp' //envoyer l'action au contrôleur
            },
            success: function (data) { // si la requête ajax est réussie
                $.each(data, function (key, val) {
                    // inclure uniquement des variables non nulles
                    if (val != null) {
                        listeAutoComp.push(val);
                        // traiter le tableau pour supprimer les valeurs en double
                        listeAutoComp = supprimeDoublons(listeAutoComp);
                    }
                });
                // appel à la fonction de saisie automatique JQueryUi
                $("#" + champAutoComp).autocomplete({
                    source: listeAutoComp
                });
            },
            error: function () { // au cas où il y aurait une erreur dans la requête ajax
                // afficher le message d'erreur
                afficheMessage(false, "Une erreur s'est produite. Veuillez réessayer");
            }
        });
    }

    /**
     * function afficheMessage
     * traiter l'affichage de tous les messages
     * @param Bool etat True si un message de réussite et False si un message d'erreur
     * @param String msg le contenu du message
     */
    function afficheMessage(etat, msg) {
        // vider le contenu du message et injecter le nouveau
        $('#resMsg').empty().text(msg);
        if (etat == false) {
            // si un message d'erreur
            $('#resMsg').css('color', 'red')
        } else {
            // si un message de réussite
            $('#resMsg').css('color', 'green')
        }
    }

    /**
     * function bordureChamp
     * ajoute une bordure entourant le champ dépendant de sa validité
     * @param Bool etat True si valid et False non valid
     * @param String selecteur de champ
     */
    function bordureChamp(etat, selecteur) {
        if (etat) { // si valeur est valid
            $(selecteur).css('border', '1px solid green')
        } else { // si non valid
            $(selecteur).css('border', '1px solid red')
        }
    }

    /**
     * function supprimeDoublons
     * supprime les valeurs en double d'un tableau
     * @param arr tableau à traiter
     * @returns {Array} tableau avec des valeurs uniques
     */
    function supprimeDoublons(arr) {
        let unique = []
        for (let i = 0; i < arr.length; i++) {
            // vérifiez si la valeur existe déjà, ajoutez-la sinon
            if (unique.indexOf(arr[i]) == -1) {
                unique.push(arr[i])
            }
        }
        return unique
    }

    /**
     * function reinitialiseFormulaire
     * réinitialise les valeurs du formulaire
     */
    function reinitialiseFormulaire() {
        $('#frmFiltre').trigger("reset");
        //masque le bouton
        $('#btnReinitialise').hide();
    }

    /**
     * function voirTous
     * afficher tous les livres disponibles
     * @param source Lien URL vers le contrôleur
     */
    function voirTous(source) {
        $.getJSON(source, function (data) {
            afficheResultat(data);
        });
        // appel pour réinitialiser le formulaire de filtre
        reinitialiseFormulaire();
        // Activer le bouton Ajouter
        $('#btnAjouteLivre').prop('disabled', false)

        $('#resMsg').empty().text('Tous les livres');
        $('#resMsg').css('color', 'black');
    }

    /**
     * function afficheResultat
     * générer dynamiquement la table qui contient le résultat des requêtes
     * @param data tableau de valeurs envoyé par la requête ajax contenant le résultat de la requête
     */
    function afficheResultat(data) {
        var resLivres = "";
        var i = 0;
        $.each(data, function (key, val) {
            i++
            // vérifie si la valeur renvoyée est nulle,
            // si c'est le cas, imprime le texte vide
            if (val['titre'] == null) {
                val['titre'] = '';
            }
            if (val['auteur'] == null) {
                val['auteur'] = '';
            }
            if (val['annee'] == null) {
                val['annee'] = '';
            }
            if (val['editeur'] == null) {
                val['editeur'] = '';
            }
            if (val['evaluation'] == null) {
                val['evaluation'] = '';
            }
            // construire les rangées de table
            resLivres += "<tr id='" + val['id'] + "'><td>" + i + "</td><td class='titre'>" + val['titre']
                + "</td><td class='auteur'>" + val['auteur'] + "</td><td class='annee'>" + val['annee']
                + "</td><td class='isbn'>" + val['isbn'] + "</td><td class='editeur'>" + val['editeur']
                + "</td><td class='evaluation'>" + val['evaluation']
                + "</td><td><span title='Supprimer' class='btnSupprimer' id='sup"
                + val['id'] + "'><i class='fas fa-minus-circle'></i></span></td></tr>"
        });
        // vider le contenu du tbody puis le remplacer par le nouveau contenu
        $("#resTab").empty().append(resLivres);
        // ajouter un écouteur d'événement click à tous les boutons de suppression
        $(".btnSupprimer").each(function () {
            $(this).click(function (e) {
                // regex pour extraire le numéro de l'identifiant
                var id = $(this).attr('id').match(/\d+/);
                supprimeLivre(parseInt(id)); // appel pour supprimer la ligne

            })
        })
        // ajouter un écouteur d'événement click à tous les champs modifiables
        $(".auteur,.isbn,.annee,.titre,.editeur,.evaluation").each(function () {
            // déclencher l'événement une fois
            $(this).one("click", function () {
                var cColonne = $(this).attr('class');
                var cIdRangee = parseInt($(this).parent().attr('id'));
                var cTexte = $(this).html();
                // appel pour modifier le champ
                editeChamp(cColonne, cIdRangee, cTexte);
            })
        })

    }

    /**
     * function validerChamps
     * valider les valeurs des champs avant d'ajouter ou de modifier le contenu de la base de données
     * @param champ le nom de colonne
     * @param valeur de champs
     * @param selecteur de champ (input)
     * @returns {boolean} True si valid et False si non valid
     */
    function validerChamps(champ, valeur, selecteur) {
        if (champ == 'isbn') {
            if (valeur == '') {// défini comme non nul dans la base de données
                afficheMessage(false, 'le champ ISBN est nécessaire');
                bordureChamp(false, selecteur);//
                return false; // non valid
            }
            if (valeur.length > 10) {
                afficheMessage(false, 'le champ ISBN accepte juste 10 caractère');
                bordureChamp(false, selecteur);
                return false;// non valid
            }
            bordureChamp(true, selecteur);
            return true // valid
        }

        if (champ == 'annee') {
            if (valeur.length > 4) {
                afficheMessage(false, 'le champ année accepte juste 4 caractère comme 2018');
                bordureChamp(false, selecteur);
                return false;
            }
            if (isNaN(parseInt(valeur))) {// tester si la valeur n'est pas un nombre
                afficheMessage(false, "s'il vous plaît entrer une année valide (0 si non connu) comme 2018.");
                bordureChamp(false, selecteur);
                return false;
            }
            bordureChamp(true, selecteur);
            return true
        }
        if (champ == 'evaluation') { // limiter la valeur aux nombres compris entre 0 et 10
            if (parseInt(valeur) < 0 || parseInt(valeur) > 10 || isNaN(parseInt(valeur))) {
                afficheMessage(false, "Evaluation invalide.Valeur doit être entre 0 (si non évalué) et 10.")
                bordureChamp(false, selecteur);
                return false;
            }
            bordureChamp(true, selecteur);
            return true
        }

        return true;
    }

    /**
     * function editeChamp
     * modifie la valeur du champ cliqué
     * @param colonne le nom de la colonne
     * @param idRangee l'identifiant de ranger
     * @param texte la valeur du champ lorsqu'on clique dessus
     */
    function editeChamp(colonne, idRangee, texte) {
        // afficher un message en modifiant le champ
        $('#resMsg').empty().text('Vous modifiez "' + texte + '" dans le champ ' + colonne);
        $('#resMsg').css('color', 'blue')

        var champEdite = '<input type="text" id="champEdite" name="champEdite">';

        // sélectionnez le td dans le tr, vide le contenu et remplace par une entrée (input)
        $('#' + idRangee + ' .' + colonne).empty().append(champEdite);

        // se concentrer sur le champ (input), peu importe la valeur
        if (texte == '') {
            $('#champEdite').val("").focus();
        } else {
            $('#champEdite').val(texte).focus();
        }

        // ajouter un écouteur d'événement en cas de perte de focus
        $('#champEdite').blur(function () {
            // prendre la nouvelle valeur du champ
            var nValeur = $('#champEdite').val();
            // envoie la nouvelle valeur à valider avant d'envoyer la requête ajax
            if (validerChamps(colonne, nValeur, '#champEdite')) {
                // créer un objet DATA dynamique
                var dataEnvoyer = {};
                dataEnvoyer['action'] = 'editeLivre'; // action pour le contrôleur
                dataEnvoyer[colonne] = nValeur;
                dataEnvoyer['id'] = idRangee;

                $.ajax({
                    url: 'ajax.php',
                    type: 'GET',
                    dataType: "json",
                    data: dataEnvoyer,
                    success: function (data) {// si la requête ajax est réussie
                        // creation de message a afficher
                        let message = data + " : de '" + texte + "' à '" + nValeur + "'";
                        afficheMessage(true, message)
                        // changer la valeur du champ à la nouvelle
                        $('#' + idRangee + ' .' + colonne).empty().append(nValeur);
                        $('#' + idRangee + ' .' + colonne).css('background-color', '#b1e9b1');

                        // réinitialiser l'evenement onclick pour cette cible
                        // car elle est déclenchée une seule fois
                        $('#' + idRangee + ' .' + colonne).one("click", function () {
                            var cColonne = $(this).attr('class');
                            var cIdRangee = parseInt($(this).parent().attr('id'));
                            var cTexte = $(this).html();
                            editeChamp(cColonne, cIdRangee, cTexte);
                        })
                    },
                    error: function () {// au cas où il y aurait une erreur dans la requête ajax
                        afficheMessage(false, "Une erreur s'est produite. Veuillez réessayer");
                    }
                });

            }
        })
    }

    /**
     * function supprimeLivre
     * supprime le livre de la base de données après confirmation
     * @param id l'identifian de rangee
     */
    function supprimeLivre(id) {
        // demander confirmation avant de supprimer
        if (confirm("Êtes-vous sûr de vouloir supprimer ce livre?")) {
            // si la réponse est oui
            $.ajax({
                url: 'ajax.php',
                type: 'GET',
                dataType: "json",
                data: {action: 'supprimeLivre', id: id},
                success: function (data) {// si la requête ajax est réussie
                    voirTous("ajax.php?action=voirTous"); // afficher tous les livres restants
                    afficheMessage(true, "le livre a été supprimé avec succès")
                },
                error: function () {// au cas où il y aurait une erreur dans la requête ajax
                    afficheMessage(false, "Une erreur s'est produite. Veuillez réessayer");
                }
            });
        }
    }


    // Fonction d'auto-appel (self invoking)
    /**
     * function filtreLivre
     * filtre les livres en fonction des critères saisis
     */
    (function filtreLivre() {
        // pour chaque champ dans formulaire de filtre
        $("#rangerFiltre input[type=text]").each(function () {
            // en perdant le focus de champ (input)
            $(this).blur(function (e) {
                var cAuteur = $("#auteur").val();
                var cTitre = $("#titre").val();
                var cAnnee = $("#annee").val();
                var cIsbn = $("#isbn").val();
                var cEditeur = $("#editeur").val();
                var cEvalution = $("#evaluation").val();

                $.ajax({
                    url: 'ajax.php',
                    type: 'GET',
                    dataType: "json",
                    data: {
                        action: 'filtreLivre',
                        auteur: cAuteur,
                        titre: cTitre,
                        annee: cAnnee,
                        isbn: cIsbn,
                        editeur: cEditeur,
                        evaluation: cEvalution
                    },
                    success: function (data) {// si la requête ajax est réussie
                        afficheResultat(data)
                        afficheMessage(true,'Resultat(s) de recherche :')
                    },
                    error: function () {// au cas où il y aurait une erreur dans la requête ajax
                        afficheMessage(false, "Une erreur s'est produite. Veuillez réessayer");
                    }
                });

                //cache le bouton de réinitialisation si les champs sont vides
                if (cAuteur == "" && cTitre == "" && cAnnee == "" &&
                    cIsbn == "" && cEditeur == "" && cEvalution == "") {
                    $('#btnReinitialise').hide();
                } else {
                    $('#btnReinitialise').show();
                }
            });
        });
    })();

    /**
     * function ajouteLivreForm()
     * ajoute des champs de formulaire en haut de la table
     * et ajoute le livre à la base de données
     */
    function ajouteLivreForm() {
        // ligne dynamique
        var champsForm = '<tr><td></td></td><td><input type="text" id="titreAjoute" name="titreAjoute"></td><td><input type="text" id="auteurAjoute" name="auteurAjoute"></td><td><input type="text" id="anneeAjoute" name="anneeAjoute" value="0" placeholder="0 si non connu"></td><td><input type="text" id="isbnAjoute" name="isbnAjoute"></td><td><input type="text" id="editeurAjoute" name="editeurAjoute"></td><td><input type="text" id="evaluationAjoute" name="evaluationAjoute" class="eval" value="0" placeholder="0 si non évalué"></td><td><span id="btnSauvegarde" title="Sauvegarder"><i class="far fa-save"></i></span></td></tr>';
        // ajoute les champs de formulaire en haut de la table
        $("#resTab").prepend(champsForm);
        // désactiver le bouton Ajouter lors de l'ajout du nouveau livre
        $('#btnAjouteLivre').prop('disabled', true)

        // ajouter un écouteur d'événement sur le clic du bouton de sauvegarde
        $('#btnSauvegarde').click(function () {
            var sAuteur = $("#auteurAjoute").val();
            var sTitre = $("#titreAjoute").val();
            var sAnnee = $("#anneeAjoute").val();
            var sIsbn = $("#isbnAjoute").val();
            var sEditeur = $("#editeurAjoute").val();
            var sEvalution = $("#evaluationAjoute").val();

            // valider les valeurs entrées
            var isbnValide = validerChamps('isbn', sIsbn, "#isbnAjoute");
            var anneeValide = validerChamps('annee', sAnnee, "#anneeAjoute");
            var evalValide = validerChamps('evaluation', sEvalution, "#evaluationAjoute");

            if (isbnValide && anneeValide && evalValide) {// si tout est valid (True)
                $.ajax({
                    url: 'ajax.php',
                    type: 'GET',
                    dataType: "json",
                    data: {
                        action: 'ajouteLivre',
                        auteur: sAuteur,
                        titre: sTitre,
                        annee: sAnnee,
                        isbn: sIsbn,
                        editeur: sEditeur,
                        evaluation: sEvalution
                    },
                    success: function () {// si la requête ajax est réussie
                        // afficher tous les livres, nouveau sur le dessus
                        voirTous("ajax.php?action=voirTous");
                        afficheMessage(true, "le livre a été ajouté avec succès");
                    },
                    error: function () {// au cas où il y aurait une erreur dans la requête ajax
                        afficheMessage(false, "Une erreur s'est produite. Veuillez réessayer");
                    }
                });
            }
        })

    };

    // ajouter un écouteur d'événement sur le clic du bouton "Voir tous les livres"
    $('#btnVoirTous').click(function () {
        // afficher tous les livres
        voirTous("ajax.php?action=voirTous");
    });
    // ajouter un écouteur d'événement sur le clic du bouton "Vider les champs"
    $('#btnReinitialise').click(function () {
        reinitialiseFormulaire();
    });
    // ajouter un écouteur d'événement sur le clic du bouton "Ajouter un livre"
    $('#btnAjouteLivre').click(function () {
        ajouteLivreForm();
        afficheMessage(true, 'Ajoute un livre:');
    });
    // ajouter un écouteur d'événement sur le clic dans chaque champ de formulaire filtre
    $('#rangerFiltre input[type=text]').each(function () {
        $(this).click(function () {
            var fColonne = $(this).attr('id');
            autoComplete(fColonne);//suggestion pour le mot filtre
        })
    });
    $(document).tooltip();  // ajouter l'info-bulle JQueryUi
    $('#btnReinitialise').hide(); // cacher le bouton de réinitialisation
    voirTous("ajax.php?action=voirTous"); // initialement afficher tous les livres disponibles
});