<?php
/**
 * Auteur: Omar Abdeljelil
 * Date:   29 juillet 2018
 * Projet: TP2- Gestion d’une collection de livres avec PHP, Mysql, jQuery et Ajax
 * Webdev: https://e1795082.webdev.cmaisonneuve.qc.ca/tp2-ajax/
 */

// définition des paramètres de connexion
define("BD_HOTE", "127.0.0.1"); // ou localhost
define("BD_UTILISATEUR", "root");
define("BD_MOTDEPASS", "1234");
define("BD_NOM", "ajaxcrudphp");
// exiger la classe PDO
require_once("pdo.class.php");
// instancier la connexion dans une variable (livreBD)
$livresBD = new PdoDB();

/**
 * function voirTous
 * une requête à la base de données pour afficher tous les livres existants
 */
function voirTous()
{
    global $livresBD;
    $livresBD->requete("SELECT * FROM livres ORDER BY id desc");
    $rows = $livresBD->resulatsDeRequetes();

    echo json_encode($rows);
}

/**
 * function autoComplete
 * vérifie s'il existe un nom de colonne dans la variable GET
 * et exécute la requête pour renvoyer des valeurs sous cette colonne
 * pour afficher en autocomplétion dans la table de filtrage
 */
function autoComplete()
{
    global $livresBD;

    $autoCompRequete = "";
    // vérifie s'il existe un nom de colonne dans la variable GET
    if (isset($_GET['champ'])) {
        $colonne = $_GET['champ'];
        // la requête est construite avec le nom de la colonne demandée
        switch ($colonne) {
            case 'auteur':
                $autoCompRequete = "SELECT auteur from livres";
                break;
            case 'titre':
                $autoCompRequete = "SELECT titre from livres";
                break;
            case 'isbn':
                $autoCompRequete = "SELECT isbn from livres";
                break;
            case 'annee':
                $autoCompRequete = "SELECT annee from livres";
                break;
            case 'editeur':
                $autoCompRequete = "SELECT editeur from livres";
                break;
            case 'evaluation':
                $autoCompRequete = "SELECT evaluation from livres";
                break;
            default:
                $autoCompRequete = '';

        }

        $livresBD->requete($autoCompRequete);
    }

    if ($autoCompRequete != "") {
        // si la requête n'est pas vide on stock les lignes dans une variable
        $rangees = $livresBD->resulatsDeRequetes();

        $liste = array();
        // nous extrayons seulement les valeurs du tableau associatif
        foreach ($rangees as $keys => $values) {
            foreach ($values as $key => $value) {
                $liste[] = $value;
            }
        }
        echo json_encode($liste);
    }

}

/**
 * function filtreResultat
 * construit une requête par plusieurs paramètres s'ils existent,
 * pour filtrer les résultats de la table
 */
function filtreResultat()
{
    global $livresBD;
    $filtreRequete = "";
    if (!empty($_GET)) {
        // cette requête initiale retournera tous les résultats de la table.
        // puisque tous les IDS sont supérieurs à zéro
        $filtreRequete .= "SELECT * FROM livres WHERE id > 0";
    }
    // alors nous ajoutons des conditions si elles ont été envoyées.
    if (isset($_GET['auteur']) && $_GET['auteur'] != "") {
        $auteur = $_GET['auteur'];
        $filtreRequete .= " and auteur like '%" . $auteur . "%'";
    }
    if (isset($_GET['titre']) && $_GET['titre'] != "") {
        $titre = $_GET['titre'];
        $filtreRequete .= " and titre like '%" . $titre . "%'";
    }
    if (isset($_GET['annee']) && $_GET['annee'] != "") {
        $annee = $_GET['annee'];
        $filtreRequete .= " and annee = '" . $annee . "'";
    }
    if (isset($_GET['isbn']) && $_GET['isbn'] != "") {
        $isbn = $_GET['isbn'];
        $filtreRequete .= " and isbn = '" . $isbn . "'";
    }
    if (isset($_GET['editeur']) && $_GET['editeur'] != "") {
        $editeur = $_GET['editeur'];
        $filtreRequete .= " and editeur like '%" . $editeur . "%'";
    }
    if (isset($_GET['evaluation']) && $_GET['evaluation'] != "") {
        $evaluation = $_GET['evaluation'];
        $filtreRequete .= " and evaluation = '" . $evaluation . "'";
    }

    if ($filtreRequete != "") {
        // nous exécutons la requête si elle n'est pas vide
        $livresBD->requete($filtreRequete);
        $rangees = $livresBD->resulatsDeRequetes();
        echo json_encode($rangees);
    }
}

/**
 * function ajouteLivre
 * exécute la requête pour ajouter un livre à la base de données
 */
function ajouteLivre()
{
    global $livresBD;
    // Définir les valeurs par défaut si elles n'ont pas été envoyées ou spécifiées
    // Tous mis à null par défaut, sauf isbn
    if (isset($_GET['auteur']) && $_GET['auteur'] != "") {
        $auteur = $_GET['auteur'];
    } else {
        $auteur = NULL;
    }
    if (isset($_GET['titre']) && $_GET['titre'] != "") {
        $titre = $_GET['titre'];
    } else {
        $titre = NULL;
    }
    if (isset($_GET['annee']) && $_GET['annee'] != "") {
        $annee = $_GET['annee'];
    } else {
        $annee = NULL;
    }
    if (isset($_GET['isbn']) && $_GET['isbn'] != "") {
        $isbn = $_GET['isbn'];
    } else {
        // isbn ne peut pas être nul, au cas où il y aurait une erreur avec la validation javascript,
        // nous allons lui donner une valeur par défaut
        $isbn = "00000000";
    }
    if (isset($_GET['editeur']) && $_GET['editeur'] != "") {
        $editeur = $_GET['editeur'];
    } else {
        $editeur = NULL;
    }
    if (isset($_GET['evaluation']) && $_GET['evaluation'] != "") {
        $evaluation = $_GET['evaluation'];
    } else {
        $evaluation = NULL;
    }

    $ajouteRequete = 'INSERT INTO livres (titre, auteur, annee, isbn, editeur, evaluation)
                values (:titre, :auteur, :annee, :isbn, :editeur, :evaluation)';
    $livresBD->requete($ajouteRequete);
    // lier les variables avec leurs valeurs
    $livresBD->bind(':titre', $titre);
    $livresBD->bind(':auteur', $auteur);
    $livresBD->bind(':annee', $annee);
    $livresBD->bind(':isbn', $isbn);
    $livresBD->bind(':editeur', $editeur);
    $livresBD->bind(':evaluation', $evaluation);

    $livresBD->execute();
    // recevoir l'identifiant de la dernière ligne insérée
    $dernierId = $livresBD->dernierIdInseree();
    // requête pour la dernière ligne insérée
    $req = "SELECT * FROM livres WHERE id = $dernierId";
    $livresBD->requete($req);
    // récupérer la dernière ligne insérée dans une variable
    $rangerInseree = $livresBD->seuleRanger();
    echo json_encode($rangerInseree);
}

/**
 * function supprimeLivre
 * supprimer une ligne par son identifiant
 */
function supprimeLivre()
{
    global $livresBD;
    if (isset($_GET['id']) && $_GET['id'] != "") {
        $id = $_GET['id'];
        $livresBD->requete("DELETE from livres WHERE id=:id");
        // lier la variable à sa valeur
        $livresBD->bind(':id', $id);
        $livresBD->execute();
        $resultat = "le livre est supprimé";
        echo json_encode($resultat);
    }
}

/**
 * function editeLivre
 * modifier un champ spécifique
 */
function editeLivre()
{
    global $livresBD;
    if (isset($_GET['id'])) {
        $id = $_GET['id'];
        // pour chaque nom de colonne, nous vérifierons s'il est configuré pour être modifié
        // si oui, nous exécutons la requête appropriée
        if (isset($_GET['auteur'])) {
            $auteur = $_GET['auteur'];
            $msg = "Vous avez modifié avec succès le champ AUTEUR";
            $editeRequete = "UPDATE livres SET auteur=:auteur WHERE id =:id";
            $livresBD->requete($editeRequete);
            $livresBD->bind(':id', $id);
            $livresBD->bind(':auteur', $auteur);
        }
        if (isset($_GET['titre'])) {
            $titre = $_GET['titre'];
            $msg = "Vous avez modifié avec succès le champ TITRE";
            $editeRequete = "UPDATE livres SET titre=:titre WHERE id =:id";
            $livresBD->requete($editeRequete);
            $livresBD->bind(':id', $id);
            $livresBD->bind(':titre', $titre);
        }
        if (isset($_GET['annee'])) {
            $annee = $_GET['annee'];
            $msg = "Vous avez modifié avec succès le champ ANNEE";
            $editeRequete = "UPDATE livres SET annee=:annee WHERE id =:id";
            $livresBD->requete($editeRequete);
            $livresBD->bind(':id', $id);
            $livresBD->bind(':annee', $annee);
        }
        if (isset($_GET['isbn']) && $_GET['isbn'] != "") {
            $isbn = $_GET['isbn'];
            $msg = "Vous avez modifié avec succès le champ ISBN";
            $editeRequete = "UPDATE livres SET isbn=:isbn WHERE id =:id";
            $livresBD->requete($editeRequete);
            $livresBD->bind(':id', $id);
            $livresBD->bind(':isbn', $isbn);
        }
        if (isset($_GET['editeur'])) {
            $editeur = $_GET['editeur'];
            $msg = "Vous avez modifié avec succès le champ EDITEUR";
            $editeRequete = "UPDATE livres SET editeur=:editeur WHERE id =:id";
            $livresBD->requete($editeRequete);
            $livresBD->bind(':id', $id);
            $livresBD->bind(':editeur', $editeur);
        }
        if (isset($_GET['evaluation'])) {
            $evaluation = $_GET['evaluation'];
            $msg = "Vous avez modifié avec succès le champ EVALUATION";
            $editeRequete = "UPDATE livres SET evaluation=:evaluation WHERE id =:id";
            $livresBD->requete($editeRequete);
            $livresBD->bind(':id', $id);
            $livresBD->bind(':evaluation', $evaluation);
        }
    }
    if ($editeRequete != "") {
        $livresBD->execute();
        // nous envoyons le message approprié
        echo json_encode($msg);
    }
}