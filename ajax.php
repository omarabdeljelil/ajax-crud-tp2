<?php
/**
 * Auteur: Omar Abdeljelil
 * Date:   29 juillet 2018
 * Projet: TP2- Gestion d’une collection de livres avec PHP, Mysql, jQuery et Ajax
 * Webdev: https://e1795082.webdev.cmaisonneuve.qc.ca/tp2-ajax/
 */
/**
 * mini-contrôleur qui exécute les fonctions du modèle
 */
require_once('livres.modele.php');
// tester la variable action
if(isset($_GET["action"]))
    $action = $_GET["action"];
else
    $action = "";
// appel aux fonctions du modèle selon la valeur de la variable 'action'
switch($action) {
    case "voirTous":
        voirTous();
        break;
    case "supprimeLivre":
        supprimeLivre();
        break;
    case "ajouteLivre":
        ajouteLivre();
        break;
    case "editeLivre":
        editeLivre();
        break;
    case "autoComp":
        autoComplete();
        break;
    case "filtreLivre":
        filtreResultat();
        break;
    default:
        break;
}