CREATE TABLE `livres` (
`id` int(10) NOT NULL auto_increment,
`titre` varchar(200),
`auteur` varchar(100),
`annee` int(4),
`isbn` varchar(10) NOT NULL,
`editeur` varchar(200),
`evaluation` int(2),
PRIMARY KEY (`id`)
);

INSERT INTO `livres` (`titre`, `auteur`, `annee`, `isbn`, `editeur`, `evaluation`) VALUES ('Harry Potter', 'JK Rollings', 1952, '15568952', 'Ameline Martell', 6);
INSERT INTO `livres` (`titre`, `auteur`, `annee`, `isbn`, `editeur`, `evaluation`) VALUES ('Les Sorcieres', 'Emmy St Marth', 1980, '5962358452', 'Britney Fostina', 2);